require("./js/input-loop.js");
global.main = main;

var frage = 0;

function main(antwort){

  if(frage == 0){

    if(antwort){

      if(antwort == "b"){
        schreibe("##### RICHTIG #####");
        nächsteFrage();
        return;
      }
      else{
        schreibe("Oh, schade, das war falsch!");
        schreibe("Probier es noch mal.");
      }

    }
    else {
      schreibe("In welcher Stadt ist die Technido Schule?");
      schreibe("a Paris");
      schreibe("b Karlsruhe");
      schreibe("c Berlin");
    }

  }

  if(frage == 1){
    if(antwort){
      schreibe( antwort + "? Ich mag " + antwort + " nicht.");
    }
    else {
      schreibe("Was ist deine Lieblingsfarbe?");
    }
  }

}

function nächsteFrage(){
  frage = frage + 1;
  main();
}

startInputLoop();
