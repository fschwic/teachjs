require("./js/input-loop.js");
global.main = main;

var obstsalat = ["Banane", "Apfel"];

function main(text){

  if( text ){
    obstsalat.push(text);
  }
  schreibe("Mein Obstsalat enthält:");
  schreibe(obstsalat);
  schreibe("Was möchtest du noch in dem Obstsalat haben.");

}

startInputLoop();
