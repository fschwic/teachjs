/*
 * ### fetch() benutzen
 *
 * Um die Funktion fetch() benutzen zu können, muss
 * das Modul node-fetch installiert werden. Führe dazu
 * den Befehl 'npm install node-fetch' im Terminal aus.
 *
 */
const fetch = require('node-fetch');

function schreibePokemon(pokemon){
  console.log("# " + pokemon.name);
  console.log(
    "Nr. " + pokemon.order +
    ", Grösse: " + pokemon.height +
    ", Gewicht: " + pokemon.weight
  );
}

fetch('https://pokeapi.co/api/v2/pokemon/ditto')
  .then(response => response.json())
//  .then(data => console.log(data));
  .then(data => schreibePokemon(data));
