var LIMIT = 200;

var prims = Array(LIMIT+1);
prims[0] = false;
for (var i = 1; i <= LIMIT; i++) {
  prims[i] = true;
}

for (var i = 2; (i*i) <= LIMIT; i++) {
  var produkt = i;
  var multiplikator = 2;
  while(produkt <= LIMIT){
    produkt = i * multiplikator;
    prims[produkt] = false;
    multiplikator++;
  }
}

for (var i = 0; i < prims.length; i++) {
  if( prims[i] ){
    console.log(i);
  }
}
