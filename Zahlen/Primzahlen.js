var LIMIT = 200;

for (var i = 3; i < LIMIT; i++) {

  // Annahme: i ist eine Primzahl
  var isPrim = true;

  for(var j = 2; j < i; j++){
    if( teilt(j, i) ){
      isPrim = false;
      break;
    }
  }

  if(isPrim){
    console.log( i );
  }

}

function teilt(a, b){
  var q = b / a;
  var p = Math.floor(q) * a;
  if(p == b){
    return true;
  }
  return false;
}

/*
function teilt(a, b){
  if( b%a ==  0){
    return true;
  }
  return false;
}
*/
