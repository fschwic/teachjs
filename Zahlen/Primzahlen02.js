var LIMIT = 200;

var prims = [];
prims[0] = "nicht prim";
for (var i = 1; i <= LIMIT; i++) {
  prims[i] = "ist prim";
}

for (var i = 2; i <= LIMIT; i++) {
  var produkt = i;
  var multiplikator = 2;
  while(produkt <= LIMIT){
    produkt = i * multiplikator;
    prims[produkt] = "nicht prim";
    multiplikator++;
  }
}

for (var i = 0; i < prims.length; i++) {
  if( prims[i] == "ist prim" ){
    console.log(i);
  }
}
