require("./js/input-loop.js");
global.main = main;

var frage = 0;

function main(antwort){

  /**************************************************************************\
  *  1. Frage (frage == 0)                                                  *
  *  In welcher Stadt ist die Technido Schule.                              *
  ***************************************************************************/
  if(frage == 0){

    if(antwort){

      if(antwort == "b"){
        schreibe("##### RICHTIG #####");
        nächsteFrage();
        return;
      }
      else{
        schreibe("Oh, schade, das war falsch!");
        schreibe("Probier es noch mal.");
      }

    }
    else {
      schreibe("In welcher Stadt ist die Technido Schule?");
      schreibe("a Paris");
      schreibe("b Karlsruhe");
      schreibe("c Berlin");
    }

  }

  /**************************************************************************\
  *  2. Frage (frage == 1)                                                  *
  *  In welcher Stadt ist die Technido Schule.                              *
  ***************************************************************************/
  if(frage == 1){

    if(antwort){
      if(antwort == "a"){
        schreibe("##### RICHTIG #####");
        nächsteFrage();
        return;
      }
      else{
        schreibe("Falsch. Die Stadt ist in Frankreich.");
        schreibe("Probier es noch mal.");
      }
    }
    else {
      schreibe("In welcher Stadt steht der Eiffelturm?");
      schreibe("a Paris");
      schreibe("b Karlsruhe");
      schreibe("c Berlin");
    }

  }

  /**************************************************************************\
  *  3. Frage (frage == 2)                                                  *
  *                                                                         *
  ***************************************************************************/
  if(frage == 2){
    schreibe("Es gibt keine dritte Frage.");
    // schreibe("Drücke ctrl+D zum Beenden.");
    exit();
  }
}

function nächsteFrage(){
  frage = frage + 1;
  main();
}

startInputLoop();
