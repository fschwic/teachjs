require("./js/input-loop.js");
global.main = main;

var o = {};
var status = 0;

function main(text){
  if(status == 0){
    schreibe("Gib deinen Vornamen ein.");
    status++;
  }
  else if(status == 1){
    o.vorname = text;
    schreibe("Gib deinen Nachnahmen ein.");
    status++;
  }
  else if(status == 2){
    o.nachname = text;
    schreibe("Wie alt bist du?");
    status++;
  }
  else if(status == 3){
    o.alter = text;
    schreibe("Wo wohnst du?");
    status++;
  }
  else if(status == 4){
    o.ort = text;
    schreibe("Was ist deine Lieblingsfarbe?")
    status++;
  }
  else if(status == 5){
    o.lieblingsfarbe = text;
    status++;
  }
  else if(status == 6){
    schreibe(o);
    schreibe(o.vorname + " " + o.nachname + " wohnt in " + o.ort + ", ist im Jahr " + (2019 - o.alter) + " geboren und hat die Lieblingsfarbe " + o.lieblingsfarbe + ".");
    exit();
  }
}

startInputLoop();
