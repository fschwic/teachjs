function getFotoAlsImg() {
  var canvas = document.createElement('canvas');//document.getElementsByTagName("canvas")[0];
  canvas.setAttribute('width', '320');
  canvas.setAttribute('height', '180');
  var ctx = canvas.getContext('2d');
  // Draws current image from the video element into the canvas
  var video = document.getElementsByTagName('video')[0];
  ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
  var img = new Image();
  img.src = canvas.toDataURL();
  return img;
}
