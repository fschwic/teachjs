require("../js/input-loop.js");
global.main = main;
global.end = zeilenAusgeben;

var count = 0;

function main(text){
  if(text){
    count++;
  }
}

function zeilenAusgeben(){
  schreibe(count + " Zeilen");
}

startInputLoop();
