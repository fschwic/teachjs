require("../js/input-loop.js");
global.main = main;

function main(text){
  if(text){
    for (var i = 0; i < text.length; i++){
      schreibe(text[i] + " vs " + text[text.length -1 - i]);
      if(text[i] != text[text.length -1 -i]){
        schreibe("Kein Palindrom.");
        return;
      }
    }
    schreibe(text + " ist ein Palindrom.\n");
  }
  schreibe("Bitte gib ein Wort ein.");
}

startInputLoop();
