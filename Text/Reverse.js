require("../js/input-loop.js");
global.main = main;

function main(text){
  if(text){
    var reverse = "";
    for (var i = text.length -1; i >= 0; i--){
      reverse += text[i];
    }
    schreibe(reverse);
    schreibe("");
  }
  schreibe("Bitte gib ein Wort ein.");
}

startInputLoop();
