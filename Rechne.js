require("./js/input-loop.js");
global.main = main;

var zahl1;
var zahl2;
var status = 1;

function main(text){

  if(status == 1){
    // Start. Frage nach der ersten Zahl.
    schreibe("Gib eine Zahl ein.");
    status++;
  }
  else if(status == 2){
    // Erste Zahl. Zahl speichern. Frage nach zweiter Zahlen.
    zahl1 = Number(text);
    schreibe("Gib eine weitere Zahl ein.");
    status++;
  }
  else if(status == 3){
    // Zweite Zahl. Zahl speichern. Frage nach Operand.
    zahl2 = Number(text);
    schreibe("Wie soll gerechnet werden?");
    schreibe("Tippe +, -, * oder /");
    status++;
  }
  else if(status == 4){
    var ergebnis;
    if(text == "+"){
      ergebnis = zahl1 + zahl2;
    }
    else if(text == "-"){
      ergebnis = zahl1 - zahl2;
    }
    else if(text == "*"){
      ergebnis = zahl1 * zahl2;
    }
    else if(text == "/"){
      ergebnis = zahl1 / zahl2;
    }
    schreibe(zahl1 + " " + text + " " + zahl2 + " = " + ergebnis.toFixed(4));
    exit();
  }

}

startInputLoop();
