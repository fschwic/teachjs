require("./js/input-loop.js");
global.main = main;

// Nummer der Frage die gerade dran ist.
var frage = 0;

function main(antwort){

  // Erste Frage
  if(frage == 0){

    if(antwort){
      // Es gibt eine Antwort. Entscheide ob richtig oder falsch.

      if(antwort == "b"){
        // Antwort b ist richtig.
        schreibe("##### RICHTIG #####");
        // Die Funktion main beenden und nächste Frage aufrufen.
        nächsteFrage();
        return;
      }
      else{
        // Antwort war nicht b. Die Antwort ist falsch.
        schreibe("Oh, schade, das war falsch!");
        schreibe("Probier es noch mal.");
      }

    }
    else {
      // Es gibt keine Antwort. Stelle die Frage.
      schreibe("In welcher Stadt ist die Technido Schule?");
      schreibe("a Paris");
      schreibe("b Karlsruhe");
      schreibe("c Berlin");
    }

  }

  // Zweite Frage
  if(frage == 1){
    if(antwort){
      // Es gibt eine Antwort.
      schreibe( antwort + "? Ich mag " + antwort + " nicht.");
    }
    else {
      // Es gibt keine Antwort.
      schreibe("Was ist deine Lieblingsfarbe?");
    }
  }

}

/* **************************************************************\
  Zur nächsten Frage gehen.

  Diese Funktion befinden sich nicht _in_ main. Sie ruft wieder
  main auf, damit die Eingabe-Ausgabe-Schleife weiter läuft.
\************************************************************** */
function nächsteFrage(){
  // Die Nummer erhöhen.
  frage = frage + 1;
  // Frage auslösen.
  main();
}

// Die Ein-/Ausgabe Schleife starten. Es wird Text eingelesen und die Funktion
// main wird aufgerufen.
startInputLoop();
