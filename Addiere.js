require("./js/input-loop.js");
global.main = main;

var zahl1;

function main(text){
  if(text){
    if( zahl1 ){
      var ergebnis = zahl1 + text;
      schreibe(zahl1 + " + " + text + " = " + ergebnis);
      exit();
    }
    zahl1 = text;
    schreibe("Gib noch eine Zahl ein.");
  }
  else {
    schreibe("Gib eine Zahl ein.");
  }
}

startInputLoop();
