require("./js/input-loop.js");
global.main = main;

var count = 0;

function main(text){
  count = count + 1;
  if(text){
    schreibe("Ja, " + text + " ist auch wirklich eine schöne Farbe.\n");
  }
  schreibe("Was ist deine Lieblingsfarbe? ");
  schreibe("Zum " + count + ". Mal. :-)");
}

startInputLoop();
