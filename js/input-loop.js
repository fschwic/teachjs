var readline = require('readline');

global.main = function(){};
global.end = function(){};

// Binde Ein- und Ausgabe an 'readline'.
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

// Event Handler bei Eingabe einer Zeile.
rl.on('line', function (zeile) {
  main(zeile);
  prompt();
});

rl.on('close', function(){
  end();
})

// Hilfsmethoden zur einfacheren Einführung
function schreibe(msg){
  //console.log("\x1b[32m", msg);
  console.log(msg);
}

function prompt(){
  process.stdout.write('> ');
}

function startInputLoop(){
  main();
  prompt();
}

function exit(){
  process.exit();
}

// Hilfsmethoden überall aufrufbar machen.
global.schreibe = schreibe;
global.prompt = prompt;
global.startInputLoop = startInputLoop;
global.exit = exit;
