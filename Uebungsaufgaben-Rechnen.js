require("./js/input-loop.js");
global.main = main;

var ANZAHL_AUFGABEN = 5;
var MAX_NUMBER = 99;

/* *****************************************************
 * Erzeuge eine bestimmte Anzahl (n) Zufallszahlen bis *
 * zu einer bestimmten Größe (max).                    *
 * Rückgabe ist ein Array mit n Zahlen.                *
 ***************************************************** */
function randomNumbers(n, max) {
  // TODO what is apply for? dense?
  var numbers = Array.apply(0, Array(n)).map(
    function() {
      return Math.floor(Math.random() * max) + 1;
    }
  );
  return numbers;
}

/* ***************************************************
 * Erzeuge eine bestimmte Anzahl (n) Zahlenpaare bis *
 * zu einer bestimmten Größe (max).                  *
 * Rückgabe ist ein Array mit n-Mal einem Array mit  *
 * jeweils zwei Zahlen.                              *
 *************************************************** */
function getPairs(n, max) {
  var list;
  var tupels = Array.apply(0, Array(n)).map(
    function() {
      return [];
    }
  );
  for (var j = 0; j < 2; j++) {
    list = randomNumbers(n, max);
    tupels = tupels.map(
      function(t, i) {
        return [...t, list[i]];
      }
    );
  }

  return tupels;
}

// Paare erstellen
var pairs = getPairs(ANZAHL_AUFGABEN, MAX_NUMBER);
// Zähler für Übungsaufgaben
var i = 0;
// Ankündigung
schreibe("Du bekommst jetzt " + ANZAHL_AUFGABEN + " Übungsaufgaben.");

/* ***********************
 * Ein-/Ausgabe-Schleife *
 *********************** */
function main(ergebnis){
  if (!ergebnis){
    schreibe(pairs[i][0] + " + " + pairs[i][1] + " =");
  }
  else{
    if( (pairs[i][0] + pairs[i][1]) == Number(ergebnis) ){
      schreibe("Richtig!");
      i++;
      if ( i == pairs.length){
        schreibe("Gut gemacht!");
        exit();
      }
    }
    else{
      schreibe("Leider falsch.");
    }
    main();
  }
}

startInputLoop();
